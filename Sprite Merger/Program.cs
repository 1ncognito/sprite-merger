﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpriteMerger
{
    class Program
    {
        static string WorkingDirectory = @"D:\Programming\Unity\Crimson Phoenix\Assets\Crimson Phoenix\Sprites\DamageSkins";
        static string DestinationDirectory = "_Atlas";
        static string[] specialTexts = new string[]
        {
            "effect",
            "miss",
            "resist",
            "counter",
            "guard",
            "shot",
        };
        static string[] extensions = new string[]
        {
            "jpg",
            "jpeg",
            "png"
        };

        static AtlasGrid CreateGrid()
        {
            var grid = new AtlasGrid();
            grid.IsDynamic = false;
            var numberCell = new CellSize()
            {
                Height = 75,
                Width = 75,
            };
            var textCell = new CellSize()
            {
                Height = 100,
                Widths = new int[] { 150, 250, 250, 350, 400, 200 }
            };
            grid.Cells = new Dictionary<int, CellSize>();
            grid.Cells.Add(0, numberCell);
            grid.Cells.Add(1, numberCell);
            grid.Cells.Add(2, textCell);

            return grid;
        }

        static void Main(string[] args)
        {
            ProcessSprites();

            Console.WriteLine("Done");
            Console.ReadKey();
        }

        static void ProcessSprites()
        {
            var grid = CreateGrid();
            var destination = Path.Combine(WorkingDirectory, DestinationDirectory);
            Directory.CreateDirectory(destination);

            foreach (var directory in Directory.EnumerateDirectories(WorkingDirectory))
            {
                if (directory == destination)
                {
                    continue;
                }

                var template = ProcessDirectorySprites(directory);
                Console.WriteLine($"Processing {template.Name}");
                using (var atlas = CombineSpritesToAtlas(template, grid))
                {
                    var resultName = Path.Combine(destination, template.Name + ".png");
                    atlas.Save(resultName, ImageFormat.Png);
                }
            }
        }

        static AtlasTemplate ProcessDirectorySprites(string path)
        {
            List<AtlasSprite> normSprites = new List<AtlasSprite>();
            List<AtlasSprite> critSprites = new List<AtlasSprite>();
            List<AtlasSprite> textSprites = new List<AtlasSprite>();

            foreach (var special in specialTexts)
            {
                textSprites.Add(default);
            }

            foreach (var file in Directory.EnumerateFiles(path))
            {
                var info = new FileInfo(file);
                var ext = info.Extension.ToLower().Substring(1);

                if (!extensions.Contains(ext))
                {
                    continue;
                }

                var name = info.Name;
                var comp = name.ToLower();
                bool specialFound = false;

                for (int i = 0; i < specialTexts.Length; i++)
                {
                    if (comp.Contains(specialTexts[i]))
                    {
                        textSprites[i] = new AtlasSprite() { File = file };
                        specialFound = true;
                        break;
                    }
                }

                if (specialFound)
                {
                    continue;
                }
                else if (comp.Contains("cri"))
                {
                    critSprites.Add(new AtlasSprite() { File = file });
                }
                else if (comp.Contains("red"))
                {
                    normSprites.Add(new AtlasSprite() { File = file });
                }
                else
                {
                    Console.WriteLine($"Could not find class for {file}");
                }
            }

            var sprites = new Dictionary<int, List<AtlasSprite>>();
            sprites.Add(0, normSprites);
            sprites.Add(1, critSprites);
            sprites.Add(2, textSprites);

            return new AtlasTemplate()
            {
                Name = new DirectoryInfo(path).Name,
                Sprites = sprites
            };
        }

        public static Bitmap CombineSpritesToAtlas(AtlasTemplate atlas, AtlasGrid grid)
        {
            Dictionary<int, List<Bitmap>> rows = new Dictionary<int, List<Bitmap>>();
            Bitmap finalImage = null;

            try
            {
                int totalWidth = 0;
                Queue<int> rowHeights = new Queue<int>();

                foreach (var row in atlas.Sprites)
                {
                    int rowWidth = 0;
                    int rowHeight = 0;
                    int column = 0;
                    List<Bitmap> rowImages = new List<Bitmap>();

                    foreach (var sprite in row.Value)
                    {
                        Bitmap bitmap = !sprite.IsEmpty ? new Bitmap(sprite.File) : null;

                        if (grid.IsDynamic && bitmap != null)
                        {
                            rowWidth += bitmap.Width;
                            rowHeight = bitmap.Height > rowHeight ? bitmap.Height : rowHeight;
                        }
                        else
                        {
                            var cell = grid.Cells[row.Key];
                            if (cell.Widths == null)
                            {
                                rowWidth += cell.Width;
                            }
                            else
                            {
                                rowWidth += cell.Widths[column];
                            }
                        }

                        rowImages.Add(bitmap);
                        column++;
                    }

                    if (rowWidth > totalWidth)
                    {
                        totalWidth = rowWidth;
                    }

                    if (grid.IsDynamic)
                    {
                        rowHeights.Enqueue(rowHeight);
                    }
                    else
                    {
                        rowHeights.Enqueue(grid.Cells[row.Key].Height);
                    }

                    rows.Add(row.Key, rowImages);
                }

                finalImage = new Bitmap(totalWidth, rowHeights.Sum());

                Graphics g = Graphics.FromImage(finalImage);
                g.Clear(Color.Transparent);

                int xOffset = 0;
                int yOffset = 0;

                foreach (var row in rows)
                {
                    int column = -1;

                    foreach (var bitmap in row.Value)
                    {
                        column++;
                        var cell = grid.Cells[row.Key];
                        if (bitmap == null)
                        {
                            var width = cell.Widths != null ? cell.Widths[column] : cell.Width;
                            xOffset += width;
                            continue;
                        }

                        g.DrawImage(bitmap, new Rectangle(xOffset, yOffset, bitmap.Width, bitmap.Height));

                        if (grid.IsDynamic)
                        {
                            xOffset += bitmap.Width;
                        }
                        else if (cell.Widths != null)
                        {
                            xOffset += cell.Widths[column];
                        }
                        else
                        {
                            xOffset += cell.Width;
                        }
                    }

                    xOffset = 0;
                    yOffset += rowHeights.Dequeue();
                }

                return finalImage;
            }
            catch (Exception ex)
            {
                if (finalImage != null)
                    finalImage.Dispose();

                throw ex;
            }
            finally
            {
                //foreach (var image in images)
                //{
                //    foreach (var bitmap in image.Value)
                //    {
                //        bitmap.Dispose();
                //    }
                //}
            }
        }
    }

    public struct CellSize
    {
        public int Width;
        public int Height;

        public int[] Widths;
    }

    public struct AtlasGrid
    {
        public bool IsDynamic;
        public Dictionary<int, CellSize> Cells;
    }

    public struct AtlasTemplate
    {
        public string Name;
        public Dictionary<int, List<AtlasSprite>> Sprites;
    }

    public struct AtlasSprite
    {
        public string File;
        public bool IsEmpty => string.IsNullOrEmpty(File);
    }
}
