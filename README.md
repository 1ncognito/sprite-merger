# Sprite Merger

Program for merging multiple sprites into one spritesheet with custom sprite positioning possibilities.

## Notes

Implementation has some hard-coded values due to there being no need for completely generic implementation, at least yet.

## History

Originally created for [Crimson Phoenix](https://bitbucket.org/1ncognito/crimson-phoenix) project. This program was used to merge its thousands of single damage skin sprites into organized atlases. Doing this reduced the amount of images to 0.04% of the original amount.